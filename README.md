## pyssh

Python script for pushing commands via SSH.

---

## About

There are three parts to this script:

1.) pyssh.py

2.) config_file.py

3.) host_file.py

*pyssh.py* will call upon and iterate through each item in the lists contained in 'config_file.py' and 'host_file.py'. 

---

## Usage
#### 1.) Define string(s) in 'config_file.py' that you want to send to the device. This can be either a single (or multiple) 'show' commands, or a sequence of configuration commands nested under 'conf t'. The idea is to emulate a human-operator. 


```python 
host_conf = [ 
             'config t',
             'ip ssh pubkey-chain',
             'no username john',
             'no username bob',
             'username alice',
             'key-hash ssh-rsa 123ABCXYZ alice@foo.com',
             'exit',
             'end',
             'wr'
            ]
```

#### 2.) Define the IP's in 'host_file.py' of the hosts you want to perform the commands on from Step 1.

 

```python
network_devices = [ 
                   '10.1.1.1',
                   '10.1.1.2'
			      ]
```

#### 3.) Execute pyssh.py

python pyssh.py
